# Diccionario de Metodologias de Desarrollo de Software

Proyecto del primer parcial para la materia de Desarrollo basado en plataformas.

## Pre-requisitos

Utilizar un sistema Unix de preferencia

## Probar el programa

Correr el script `diccionario_metodologias.sh -a` sobre la terminal para metodologías ágiles 

```
diccionario_metodologias.sh -a
```

Correr el script `diccionario_metodologias.sh -t` sobre la terminal para metodologías tradicionales 

```
diccionario_metodologias.sh -t
```

## Herramientas Usadas 

El proyecto está desarrollado en bash script, con apoyo de herramientas de trabajo como Git y Docker

## Versiones


**Las versiones y avances pueden revisarse en los diferentes commits**

## Autores
1. [Armando Chavez Perez](https://github.com/Armandochavezp02)*316099*
2. [Juan Daniel Villegas Terrazas](https://github.com/JuanDanielVillegas) *329545*
3. [Javier Andres Tarango Fierro](https://github.com/329904) *329904*
